//#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//using namespace std;

int main()
{
    int i=10;
    clock_t start,finish;
    double Times, Times1;
    start=clock();
    while(i--){
        cout<<i<<endl;
    };
    finish=clock();
    Times=(double)(finish-start)/CLOCKS_PER_SEC;
    Times1=(double)(finish-start)/CLK_TCK;
    cout<<"inicio (reloj):"<<start<<endl;
    cout<<"terminar (ajuste del reloj):"<<finish<<endl;
    cout<<"CLOCKS_PER_SEC: "<<CLOCKS_PER_SEC<<endl;
    cout<<"CLK_TCK: "<<CLK_TCK<<endl;
    cout<<"Tiempo de ejecución (segundos) (CLOCKS_PER_SEC):"<<Times<<endl;
    cout<<"Tiempo de ejecución (segundos) (CLK_TCK):"<<Times1<<endl;

    return 0;
}
